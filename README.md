# LEE SHEPPARD

The code enclosed with this repository is the property of Lee Sheppard and has been completed in response 
to a code challenge made for employment means only. It is not meant to be copied or distributed for any other means. 

![Lee Sheppard signature](http://res.cloudinary.com/leesheppard/image/upload/v1496495524/Lee-Sheppard-Black_iv1j84.png)

# Social
[![Hello](https://img.shields.io/badge/Hello-%40leesheppard-blue.svg)](https://twitter.com/leesheppard)

- [Linkedin](https://www.linkedin.com/in/leesheppard)
- [Twitter](https://twitter.com/leesheppard)
- [Dribbble](https://dribbble.com/leesheppard)
- [Instagram](https://instagram.com/leesheppard)

<hr />

## Configuration

Clone repo from here and install:
```bash
$ git clone git@gitlab.com:leesheppard/pinetree-knead-one.git 
$ cd pinetree-knead-one
$ yarn install
```

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

<hr />

# The Problem
We (Linktree) have three new link types for our users profile pages.

1. Classic
    - Accepts a single URL.
    - Opens the URL in a new tab.

2. Music Player
    - On click, the link type displays a set of links to music streaming platforms where you can find the song.
    - Clicking on an element inside of the Music Link will open an audio player for that song, powered by that platform.
    - Clicking on the logo of the platform of an element will take you to that song, in that platform.

3. Shows List
    - On click, the link type displays a list of links to upcoming events.
    - Clicking on an upcoming event will navigate to the event in SongKick.

You are required to create the front end components for the new features as per the assets provided via Zeplin (Front End Assets)

## Design Considerations
- All links are globally themed by the users preferences (usually available via API e.g `{background_color: "rebeccapurple", text_color:  "palegoldenrod" }`).
- All links invert colour on hover.

## Your Solution

- Consider re-usability through composition.


## Rules & Tips
- Use of ReactJS will be looked upon favourably.
- You cannot connect to a real world API - mock any data sets that you require.
- @todo comments are encouraged. You aren't expected to complete the challenge, but how you design your solution and your ideas for the future are important.
- Commit your work and submit Pull Requests as you would in real life.

