import './App.scss'
import UserHeader from './components/User/UserHeader'
import Branding from './components/Branding/Branding'
import LinkContainer from './components/Buttons/LinkContainer'
import LinkContent from './components/Buttons/LinkContent'

// TODO loop over LinkContainer to check custom links and category with toggle
// TODO Current links below for mocking layout purpose only, remove when looping completed
// TODO Requires more styling for containers, show layout and music layouts

function App() {
  return (
    <div className="App">
      <div className="mobile-wrap">
        <UserHeader />
        <div className="link-wrapper">
          <LinkContainer>
            <LinkContent />
          </LinkContainer>
          <LinkContainer linkAdvanced={false}>
            <LinkContent />
          </LinkContainer>
          <LinkContainer linkAdvanced={true}>
            <LinkContent />
          </LinkContainer>
          <LinkContainer>
            <LinkContent />
          </LinkContainer>
          <LinkContainer>
            <LinkContent />
          </LinkContainer>
        </div>
        <Branding brandurl="https://www.linktr.ee" brandname="Linktree" />
      </div>
    </div>
  )
}

export default App
