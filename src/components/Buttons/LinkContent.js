import React from 'react'
import './LinkContent.scss'

// TODO - pass in user profile settings, links
// TODO - conditional to loop over for link category types - classic, show, music
//        with each condition to create link appropriate (LinkMusicPlayer, LinkShowsList)
// TODO - styling for show, music layouts

function LinkContent(props) {
  return (
    <div>
      <a
        className="link-content"
        href={props.linkurl}
        target="_blank"
        rel="noreferrer noopener"
      >
        {props.linkname}
      </a>
    </div>
  )
}

export default LinkContent
