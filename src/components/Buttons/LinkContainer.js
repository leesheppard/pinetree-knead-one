import React from 'react'
import './LinkContainer.scss'
import LinkContent from './LinkContent'

// TODO - allow pass in of link category type; classic, show, music
//        conditional will bring in LinkMusicPlayer or LinkShowsList
//        with default for Classic as backup

function LinkContainer(props) {
  let { linkAdvanced } = props

  switch (linkAdvanced) {
    case true:
      return (
        <div className="link-container">
          <LinkContent linkname={'Music'} linkurl={'#'} />
        </div>
      )
    case false:
      return (
        <div className="link-container">
          <LinkContent linkname={'Show'} linkurl={'#'} />
        </div>
      )
    default:
      return (
        <div className="link-container">
          <LinkContent linkname={'Link'} linkurl={'#'} />
        </div>
      )
  }
}

export default LinkContainer
