import React from 'react'
import userAvatar from '../../assets/leesheppard_black.jpg'
import './UserAvatar.scss'

function UserAvatar(props) {
  return (
    <div>
      <img
        className="user-avatar"
        src={userAvatar}
        alt={props.name}
        width="64px"
      />
    </div>
  )
}

export default UserAvatar
