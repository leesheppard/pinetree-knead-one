import React from 'react'
import './UserHeader.scss'
import UserName from './UserName'
import UserAvatar from './UserAvatar'
import UserBio from './UserBio'

function UserHeader() {
  return (
    <div className="userBanner">
      <UserAvatar />
      <UserName username="@LeeSheppard" userlink="https://leesheppard.com" />
      <UserBio />
    </div>
  )
}

export default UserHeader
