import React from 'react'
import './UserName.scss'

// TODO - pass in username, whitelabel, premium options, color settings etc.

function UserName(props) {
  return (
    <div>
      <a
        className="username"
        href={props.userlink}
        target="_blank"
        rel="noreferrer noopener"
      >
        <p className="username-text">{props.username}</p>
      </a>
    </div>
  )
}

export default UserName
