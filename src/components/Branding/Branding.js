import React from 'react'
import brand from '../../brand/logo.svg'
import './Branding.scss'

// TODO brand needs a flag to hide for white labelling

function Branding(props) {
  return (
    <div className="Branding">
      <a href={props.brandurl} target="_blank" rel="noreferrer noopener">
        <img src={brand} alt={props.brandname} className="brand-logo" />
      </a>
    </div>
  )
}

export default Branding
