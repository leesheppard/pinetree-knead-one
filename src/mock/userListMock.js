import mock from '../utils/mock';mock.onGet('/api/user/list').reply(200, { users: [{"id":1,"first_name":"Janette","last_name":"Sennett","website":"https://amazon.co.uk","background_color":"Blue","text_color":"Maroon"},
{"id":2,"first_name":"Theressa","last_name":"Yurygyn","website":"https://bing.com","background_color":"Pink","text_color":"Orange"},
{"id":3,"first_name":"Quintin","last_name":"Smithend","website":"https://123-reg.co.uk","background_color":"Mauv","text_color":"Violet"},
{"id":4,"first_name":"Glen","last_name":"Lehrer","website":"http://amazon.com","background_color":"Indigo","text_color":"Teal"},
{"id":5,"first_name":"Wolf","last_name":"O'Scanlan","website":"https://surveymonkey.com","background_color":"Yellow","text_color":"Green"}]})

